target := sample

all: $(target).pdf

%.pdf: %.md emoji.lua
	pandoc \
		--pdf-engine=lualatex \
		--lua-filter=emoji.lua \
		-s -o $@ $<

clean:
	$(RM) $(target).pdf
