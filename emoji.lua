-- locate 'emoji-table.def' of the 'emoji' latex package where the emoji table is stored
local cmd_emoji_table = "kpsewhich -engine=luatex emoji-table.def"
local output = io.popen(cmd_emoji_table)
if not output then
    error("Can not locate emoji-table.def via this command: " .. cmd_emoji_table)
end
local filename = output:read("*line")
output:close()

-- read 'emoji-table.def' and store emoji names in table
local emojis = {}
local file = io.open(filename, "r")
if not file then
    error("Can not open file: " .. filename)
end
for line in file:lines() do
    local code, name = string.match(line, "\\__emoji_def:nnnnn {([^}]+)} {([^}]+)}")
    if code and name then
        local emoji = ''
        for word in string.gsub(code, "%^", " "):gmatch("%S+") do
            if word then
                emoji = emoji .. utf8.char(tonumber(word, 16))
            end
        end
        table.insert(emojis, {emoji, name})
    end
end
file:close()

-- sort table such that long emoji are matched first
table.sort(emojis, function(a, b) return #a[1] > #b[1] end)

-- constants
local pandoc = require('pandoc')
local OPEN = "\\text{\\emoji{"
local CLOSE = "}}"

local function replace(s, wrap)
    for _, tuple in ipairs(emojis) do
        s = string.gsub(s, tuple[1], wrap and OPEN..tuple[2]..CLOSE or tuple[2])
    end
    return s
end

function Str(elem)
    local text = replace(elem.text)
    if text ~= elem.text then
        return pandoc.RawInline("latex", OPEN..text..CLOSE)
    end
    return elem
end

function Math(elem)
    elem.text = replace(elem.text, true)
    return elem
end

function Meta(meta)
    local block = pandoc.RawBlock("latex", "\\usepackage{emoji}")
    if meta['header-includes'] then
        table.insert(meta['header-includes'], block)
    else
        meta['header-includes'] = pandoc.MetaBlocks({block})
    end
    return meta
end
