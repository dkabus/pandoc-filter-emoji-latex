---
title: Emoji in LaTeX
author: Desmond Kabus
date: 2023-03-27
header-includes: |
    ```{=latex}
    \usepackage{emoji}
    \setemojifont{TwemojiMozilla}
    ```
...

Bla bla bla ❤️

$$
📈\left(⏲️, {\vec💘}\right)
=
\int_❤️
\frac{
    \nabla \cdot 🌀\left({\vec💘}\right) \nabla ⚡\left(⏲️, {\vec💘}\right)
}{
    \left({\vec💘}  - {{\vec💘}}'\right)^2
}
\,\mathup{d}{{\vec💘}}'
$$
